# Add Strava authentication to your app

> <a href="https://nicolas-dmg.medium.com/from-start-to-finish-integrating-strava-api-in-next-js-d08474ff69b7" rel="noopener noreferrer" target="_blank">From Start to Finish: Integrating Strava API in Next.js</a>

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

---

## About

This repository is the code sample of a Medium article. You can read it <a href="https://nicolas-dmg.medium.com/from-start-to-finish-integrating-strava-api-in-next-js-d08474ff69b7" rel="noopener noreferrer" target="_blank">here</a>.

---

## Installation

```bash
git clone git@gitlab.com:dommangetnicolas/nextjs-strava.git
cd nextjs-strava
nvm use
npm i
```

Create an environment file `.env.development.local`
```
NEXT_PUBLIC_STRAVA_CLIENT_ID=<value>
NEXT_PUBLIC_STRAVA_REDIRECT_URI=<value>
STRAVA_CLIENT_SECRET=<value>
JWT_KEY=<value>
JWT_EXPIRES_IN=<value>
```

---

## Usage

```bash
npm run dev
```

---

## Contributing

Do not hesitate to fork the project and create a pull request. When creating a PR, please comment it (So I can edit the article).

---

## Support

Reach out to me at one of the following places!

- Website at <a href="https://nicolas-dmg.fr/" rel="noopener noreferrer" target="_blank">`nicolas-dmg.fr`</a>
- Email at <a href="mailto:contact@nicolas-dmg.fr?subject=Hey! Are you available?">contact@nicolas-dmg.fr</a>

---

## About me

<table style="border: none;">
  <tr>
    <td>
      <div style="width: 120px;">
        <img width="120" src="https://avatars1.githubusercontent.com/u/46563166?s=460&u=8d851cf38c28b0f78cbacdccaa9f332e73687f52&v=4"/>
    </div>
    </td>
    <td>
      <div style="margin-left: 30px;">
        <p>Hey there !</br>
        I am Nicolas, Software engineer <a href="https://mailjet.com/">@mailjet</a> the day, side projects the night.
        </br>
        If you have any question you can <a href="https://www.linkedin.com/in/nicolas-dommanget-muller/">contact me</a> 😃.</p>
        <p>I'm always ready to help !</p>
        <a href="mailto:contact@nicolas-dmg.fr?subject=Hey! Are you available?">Email me</a>
        &nbsp;
        <a href="https://dev.nicolas-dmg.fr/" rel="noopener noreferrer" target="_blank">Visit my website</a>
        &nbsp;
        <a href="http://go.nicolas-dmg.fr/in" rel="noopener noreferrer" target="_blank">Connect with me</a>
    </div>
    </td>
  </tr>
</table>

---

## License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2019-2022 © <a href="https://nicolas-dmg.fr/" target="_blank">Nicolas Dommanget-Muller</a>.